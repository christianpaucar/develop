package main

import "fmt"

func main() {
	//una de las formas es con go rutinas enviar
	//y recibir datos
	c := make(chan int)

	go func() {
		c <- 52
	}()

	fmt.Println(<-c)

	//otra manera es mediante canales de buffer
	c1 := make(chan int, 1)

	c1 <- 55

	fmt.Println(<-c1)
}
