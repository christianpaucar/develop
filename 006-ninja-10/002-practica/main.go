package main

import "fmt"

func main() {
	//canal bidireccional
	//send only
	//cs := make(chan<- int)
	cs := make(chan int)

	go func() {
		cs <- 145
	}()

	fmt.Println(<-cs)
	fmt.Println("")
	fmt.Printf("%T", cs)

	//canal bidireccional
	//recive only
	//cs1 := make(<-chan int)
	cs1 := make(chan int)
	go func() {
		cs1 <- 145
	}()

	fmt.Println(<-cs1)
	fmt.Println("")
	fmt.Printf("%T", cs1)

}
