package main

import (
	"fmt"
)

type errorPer struct {
}

func (ep errorPer) Error() string {
}

func main() {
	e1 := errorPer{}
	foo(c1)
}

func foo(e error) {
	fmt.Println(e)
}
