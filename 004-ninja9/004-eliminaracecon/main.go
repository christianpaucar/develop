package main

import (
	"fmt"
	"sync"
)

func main() {
	var wg sync.WaitGroup
	contador := 0
	con := 100
	wg.Add(con)
	var m sync.Mutex

	for i := 0; i < con; i++ {
		go func() {
			m.Lock()
			v := contador
			v++

			contador = v

			fmt.Println(contador)
			m.Unlock()
			wg.Done()
		}()

	}
	wg.Wait()
	fmt.Println(contador)

}
