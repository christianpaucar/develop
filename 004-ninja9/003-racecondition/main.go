package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {
	fmt.Println("numero de cpus :", runtime.NumCPU())
	fmt.Println("numero de gorutinas:", runtime.NumGoroutine())
	fmt.Println("race condition")
	contador := 0
	con := 100
	var wg sync.WaitGroup
	wg.Add(con)

	for i := 0; i < con; i++ {
		go func() {
			v := contador
			v++
			runtime.Gosched()

			contador = v
			fmt.Println(runtime.NumGoroutine())
			wg.Done()
		}()

		fmt.Println("el valor del incremento final ///", runtime.NumGoroutine())
		wg.Wait()
	}

}
