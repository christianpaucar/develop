package main

import (
	"fmt"
	"runtime"
)

func main() {
	fmt.Println("sistema operativo", runtime.GOOS)
	fmt.Println("arquitectura del sistema", runtime.GOARCH)
}
