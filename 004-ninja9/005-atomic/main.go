package main

import (
	"fmt"
	"sync"
	"sync/atomic"
)

func main() {
	var wg sync.WaitGroup
	var contador int64

	con := 100
	wg.Add(con)

	for i := 0; i < con; i++ {
		go func() {

			atomic.AddInt64(&contador, 1)
			fmt.Println(atomic.LoadInt64(&contador))
			wg.Done()
		}()

	}
	wg.Wait()
	fmt.Println(contador)

}
