package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {
	fmt.Printf("numerdo de cpu's:%v", runtime.NumCPU())
	fmt.Println("-------")

	var wg sync.WaitGroup
	wg.Add(2)
	go func() {
		fmt.Println("go rutina 1")
		wg.Done()
	}()
	go func() {
		fmt.Println("go rutina 2")
		wg.Done()
	}()

	fmt.Printf("numerdo de cpu's:%v", runtime.NumCPU())
	fmt.Println("-------")
	fmt.Printf("numerdo de gorutinas:%v", runtime.NumGoroutine())
	fmt.Println("-------")
	wg.Wait()
	fmt.Println("estoy a punto de culminar")

	fmt.Printf("numerdo de cpu's:%v", runtime.NumCPU())
	fmt.Println("-------")
	fmt.Printf("numerdo de gorutinas:%v", runtime.NumGoroutine())
	fmt.Println("-------")
}
