package main

import "fmt"

type persona struct {
	nombre   string
	apellido string
	edad     int
}

func (p *persona) hablar() {
	fmt.Println("hola mi nombre es", p.nombre)
}

type humano interface {
	hablar()
}

func dialogo(h humano) {
	h.hablar()

}

func main() {
	p1 := persona{
		nombre:   "fernado",
		apellido: "paucar",
		edad:     27,
	}

	dialogo(&p1)
	fmt.Println("hey he y ehy")

}
