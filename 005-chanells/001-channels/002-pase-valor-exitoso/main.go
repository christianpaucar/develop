package main

import "fmt"

func main() {
	//canal sin bufer recibo con go rutina
	ca := make(chan int)

	go func() {
		ca <- 52
	}()
	fmt.Println(<-ca)
}
