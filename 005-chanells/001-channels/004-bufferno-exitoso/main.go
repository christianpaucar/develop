package main

import "fmt"

func main() {
	//canal con buffer
	ca := make(chan int, 1)

	ca <- 52
	ca <- 52

	fmt.Println(<-ca)
}
