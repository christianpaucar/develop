package main

import "fmt"

func main() {
	//canal con buffer
	ca := make(chan<- int, 2)

	ca <- 52
	ca <- 55

	fmt.Println(<-ca)
	fmt.Println(<-ca)
	fmt.Printf("%T", ca)

}
