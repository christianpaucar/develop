package main

import "fmt"

func main() {
	c := make(chan int)
	//envio
	go func() {
		for i := 0; i < 50; i++ {
			c <- i
		}
		//cierro el canal
		close(c)
	}()

	//recibir
	for v := range c {
		fmt.Println(v)
	}
	fmt.Println("finalizando")
}
