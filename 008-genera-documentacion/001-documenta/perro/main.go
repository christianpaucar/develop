package main

import (
	"fmt"

	"gitlab.com/christianpaucar/develop/008-genera-documentacion/001-documenta/perro"
)

type cannino struct {
	nombre string
	edad   int
}

func main() {
	c := canino{
		nombre: "skady",
		edad:   perro.Edad(3),
	}
	fmt.Println(c)

}
